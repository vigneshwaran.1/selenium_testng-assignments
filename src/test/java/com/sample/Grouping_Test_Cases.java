package com.sample;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Grouping_Test_Cases {
	@Test(groups = { "Smoke Test" })
	public void createCustomer() {

		System.out.println("Customer will get created");
	}

	@Test(groups = { "Regression Test" })
	public void newCustomer() {

		System.out.println("New Customer will get created");
	}

	@Test(groups = { "Usabilty Test" })
	public void modifyCustomer() {

		System.out.println("Customer will get modified");

	}

	@Test(groups = { "Smoke Test" })
	public void changeCustomer() {

		System.out.println("Customer will get been changed");
	}

	@BeforeClass

	public void beforeClass() {

		System.out.println("Start Data base connection,Start browser");

	}

	@AfterClass
	public void afterClass() {

		System.out.println("Close Data base Connection,Close Browser");
	}
}
