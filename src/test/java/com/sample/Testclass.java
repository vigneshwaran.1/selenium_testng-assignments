package com.sample;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Testclass {
	@Test(priority=1)
	public void modifyCustomer() {

		System.out.println("Customer will get modified");
	}

	@Test(priority=3)
	public void createCustomer() {

		System.out.println("Customer will get created");
	}

	@Test(priority=2)
	public void newCustomer() {

		System.out.println("New Customer will get created");
	}
	
	@BeforeMethod
	  public void beforeCustomer() {
		  
		  System.out.println(" verify the Customer");
	  }
	@AfterMethod
	  public void afterCustomer() {
		  
		  System.out.println("All Transactions are done");
	  }
	
	@BeforeClass
	
	  public void beforeClass() {
		  
		  System.out.println("Start Data base connection,Start browser");
		  
	  }
	@AfterClass
	  public void afterClass() {
		  
		  System.out.println("Close Data base Connection,Close Browser");
	  }

}
// Before class Executed on Beginning
// After class Executed On ending
//BeforeMethod Executed on before on every Testclass execution.
//AfterMethod Executed on after every Testclass Execution
